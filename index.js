var numberArray = [];
function themSo() {
  var inputEl = document.querySelector("#txt-number");
  if (inputEl.value == "") {
    return;
  }
  var number = inputEl.value * 1;
  numberArray.push(number);
  inputEl.value = "";
  document.getElementById("output").innerHTML = numberArray;

  var sum = 0;
  var positiveCount = 0;
  var min = numberArray[0];
  var even = 0;
  var positiveMin = numberArray[0];
  var negativeCount = 0;
  var compare;
  numberArray.sort();

  for (var i = 0; i <= numberArray.length; i++) {
    var number = numberArray[i];
    if (number > 0) {
      sum += number;
      positiveCount++;
    }
    if (min > numberArray[i]) {
      min = numberArray[i];
    }
    if (number % 2 == 0) {
      even = number;
    }
    if (positiveMin > numberArray[i] && numberArray[i] > 0) {
      positiveMin = numberArray[i];
    }
    if (number < 0) {
      negativeCount++;
    }
    if (positiveCount > negativeCount) {
      compare = `Số lượng âm ít hơn số lượng dương`;
    }
    if (positiveCount < negativeCount) {
      compare = `Số lượng âm nhiều hơn số lượng dương`;
    }
    if (positiveCount == negativeCount) {
      compare = `Số lượng âm bằng số lượng dương`;
    }
  }

  document.getElementById("sum").innerHTML = sum;
  document.getElementById("positive").innerHTML = positiveCount;
  document.getElementById("min").innerHTML = min;
  document.getElementById("even").innerHTML = even;
  document.getElementById("positiveMin").innerHTML = positiveMin;
  document.getElementById("swapUp").innerHTML = numberArray;
  document.getElementById("compare").innerHTML = compare;
}
function doiSo() {
  var first = +document.getElementById("first").value;
  var second = +document.getElementById("second").value;

  var trungGian = numberArray[first];
  numberArray[first] = numberArray[second];
  numberArray[second] = trungGian;

  document.getElementById("doiSo").innerHTML = ` ${numberArray}`;
}

function checkPrime(number) {
  if (number == 1) {
    return false;
  } else if (number > 1) {
    for (let i = 2; i < number; i++) {
      if (number % i == 0) {
        return false;
      }
    }
    return true;
  }
}

function timSoNguyenTo() {
  for (var i = 0; i < numberArray.length; i++) {
    if (checkPrime(numberArray[i]) == true) {
      document.getElementById("soNguyenTo").innerHTML = numberArray[i];
      break;
    } else {
      document.getElementById("soNguyenTo").innerHTML = `-1`;
    }
  }
}
function demSoNguyen() {
  var number = document.getElementById("numbers").value * 1;
  numberArray.push(number);
  var integer = 0;
  for (var i = 0; i < numberArray.length; i++) {
    if (number.isInterger(numberArray[i]) == true) {
      integer++;
    }
    document.getElementById("soNuyen").innerHTML = integer;
  }
}
